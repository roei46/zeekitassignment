//
//  Category.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 10/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "Category.h"

@implementation Category


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@,%@,%@,%@", self.category,self.image_url,@(self.priority),self.name];
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    //do nothing
}
@end
