//
//  CollectionViewDataSource.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 10/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "CollectionViewDataSource.h"
#import "Category.h"
#import "CollectionViewCell.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CollectionViewDataSource () <UICollectionViewDelegate>
@property(strong, nonatomic) NSMutableArray *categoryArr;

@end
@implementation CollectionViewDataSource

#pragma mark - UICollectionViewDataSource Methods
static NSString *CellIdentifier = @"MyCollectionCell";

- (instancetype)initWithCategory:(NSMutableArray *)category {

  self = [super init];
  if (self) {
    self.categoryArr = [[NSMutableArray alloc] init];
    [self.categoryArr addObjectsFromArray:category];
  }
  return self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {

  return [self.categoryArr count];
}

- (void)fillCollectionCellWithData:(NSMutableArray *)arr {

  [self.categoryArr removeAllObjects];
  [self.categoryArr addObjectsFromArray:arr];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {

  CollectionViewCell *cell = (CollectionViewCell *)[collectionView
      dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                forIndexPath:indexPath];

  Category *category = [self.categoryArr objectAtIndex:indexPath.row];
  cell.category.text = category.name;
  cell.priority.text =
      [TEXT_PRIORITY stringByAppendingString:@(category.priority).stringValue];
    

  [cell.imgView sd_setImageWithURL:[NSURL URLWithString:category.image_url]
                  placeholderImage:[UIImage imageNamed:@"placeHolder.png"]
                           options:SDWebImageRefreshCached];

  return cell;
}

- (id)categoryAtIndexPath:(NSIndexPath *)indexPath {
  return [self.categoryArr objectAtIndex:indexPath.row];
}
@end
