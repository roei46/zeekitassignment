//
//  ZeekitApi.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ZeekitApi.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZeekitApi

static ZeekitApi *sharedInstance = nil;
static dispatch_once_t onceToken;

+ (ZeekitApi *)sharedInstance {

  dispatch_once(&onceToken, ^{
    sharedInstance = [[self alloc] init];
  });
  return sharedInstance;
}

- (void)
requestDataWithPostCount:(NSInteger)postCount
                 success:
                     (nullable void (^)(NSMutableArray *_Nonnull result))success
                 failure:(nullable void (^)(NSError *_Nullable e))failure {

  if (postCount < 1 || postCount > 4) {
    // no such stuff
    return;
  }
    
    NSLog(@"link:%@",[NSString stringWithFormat:ZEEKIT_REQUEST_URL, @(postCount)]);
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager POST:[NSString stringWithFormat:ZEEKIT_REQUEST_URL, @(postCount)]
      parameters:nil
      progress:nil
      success:^(NSURLSessionTask *task, id responseObject) {
        success(responseObject);
      }
      failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
      }];
}

@end
