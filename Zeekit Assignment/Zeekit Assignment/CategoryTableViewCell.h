//
//  CategoryTableViewCell.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Category;
@protocol OnImageSelectedInCollectionView
- (void)onCategorySelected:(Category *) category;
@end
@interface CategoryTableViewCell : UITableViewCell
@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (void)setDataSource:(id<UICollectionViewDataSource>)dataSource;
@property(nonatomic, strong) id<OnImageSelectedInCollectionView> delegate;

@end
