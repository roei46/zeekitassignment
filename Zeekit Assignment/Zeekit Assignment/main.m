//
//  main.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
