//
//  Category.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 10/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property(nonatomic, strong) NSString *category;
@property(nonatomic, strong) NSString *image_url;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, assign) NSInteger priority;
@end
