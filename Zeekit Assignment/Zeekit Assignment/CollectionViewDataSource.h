//
//  CollectionViewDataSource.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 10/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class Category;
@interface CollectionViewDataSource : NSObject <UICollectionViewDataSource>

- (void)fillCollectionCellWithData:(NSMutableArray *)arr;
- (instancetype)initWithCategory:(NSMutableArray *)category;
- (Category *)categoryAtIndexPath:(NSIndexPath *)indexPath;

@end
