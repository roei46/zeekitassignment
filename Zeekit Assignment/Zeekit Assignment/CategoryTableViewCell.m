//
//  CategoryTableViewCell.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "CategoryTableViewCell.h"
#import "CollectionViewCell.h"
#import "CollectionViewDataSource.h"
@interface CategoryTableViewCell () <UICollectionViewDelegate>

@end

@implementation CategoryTableViewCell

- (void)awakeFromNib {
  [super awakeFromNib];
  // Initialization code
  self.collectionView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)setDataSource:(id<UICollectionViewDataSource>)dataSource {
  self.collectionView.dataSource = dataSource;
}

- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

  CollectionViewDataSource *ds = self.collectionView.dataSource;
  Category *category = [ds categoryAtIndexPath:indexPath];
  [self.delegate onCategorySelected:category];
}
@end
