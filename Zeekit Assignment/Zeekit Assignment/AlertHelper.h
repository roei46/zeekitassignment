//
//  Constants.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 10/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "Constants.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AlertHelper : NSObject

+ (nonnull instancetype)sharedInstance;

+ (void)showAlertWithDefaultErrorMessage;

+ (void)showAlertWithTitle:(nonnull NSString *)title
                andMessage:(nonnull NSString *)message;

@end
