//
//  Constants.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define ZEEKIT_REQUEST_URL @"http://qa.zeekit.co/post_%@.json"

#define TEXT_PRIORITY @"Blog priority: "

#define TEXT_ALERT_OK @"OK"
#define TEXT_ALERT_CANCEL @"Cancel"
#define TEXT_ALERT_ERROR_TITLE @"Error!"
#define TEXT_ALERT_ERROR_MESSAGE                                               \
  @"Oops! Something went wrong. Please try again."

#endif /* Constants_h */
