//
//  ModalViewController.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 11/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ViewController.h"
#import "Category.h"

@interface ModalViewController : ViewController
@property(weak, nonatomic) IBOutlet UIImageView *imgView;
@property(weak, nonatomic) IBOutlet UILabel *name;
@property(weak, nonatomic) Category *category;


@end
