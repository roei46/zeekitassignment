//
//  ZeekitApi.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZeekitApi : NSObject

+ (ZeekitApi *_Nonnull)sharedInstance;

- (void)
requestDataWithPostCount:(NSInteger)postCount
                 success:
                     (nullable void (^)(NSMutableArray *_Nonnull result))success
                 failure:(nullable void (^)(NSError *_Nullable e))failure;

@end
