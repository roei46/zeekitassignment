//
//  CollectionViewCell.h
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property(weak, nonatomic) IBOutlet UIImageView *imgView;
@property(weak, nonatomic) IBOutlet UILabel *category;
@property(weak, nonatomic) IBOutlet UILabel *priority;

@end
