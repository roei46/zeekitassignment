//
//  ViewController.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 09/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ViewController.h"
#import "AlertHelper.h"
#import "Category.h"
#import "CategoryTableViewCell.h"
#import "CollectionViewCell.h"
#import "CollectionViewDataSource.h"
#import "Constants.h"
#import "ModalViewController.h"
#import "ZeekitApi.h"
#import <AFNetworking/AFNetworking.h>

@interface ViewController () <UITableViewDelegate, UITableViewDataSource,
                              OnImageSelectedInCollectionView>
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(weak, nonatomic) NSMutableArray *arr;
@property(strong, nonatomic) NSMutableDictionary *dic;
@property(strong, nonatomic) NSMutableArray *categoryArr;
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(weak, nonatomic) IBOutlet UIImageView *zeekitImageView;
@property(nonatomic, assign) NSInteger postCount;
@property(weak, nonatomic) IBOutlet UILabel *titleLbl;
@end

static NSString *CellIdentifier = @"MyCell";
static NSString *CollectionCellIdentifier = @"MyCollectionCell";

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
  self.tableView.separatorColor = [UIColor whiteColor];
  self.tableView.hidden = YES;
  self.titleLbl.hidden = YES;
  self.postCount = 1;
}

- (IBAction)fetchData:(id)sender {
  [self.activityIndicator startAnimating];
  [[ZeekitApi sharedInstance] requestDataWithPostCount:self.postCount
      success:^(NSMutableArray *_Nonnull result) {
        self.categoryArr = [[NSMutableArray alloc] init];
        self.dic = [[NSMutableDictionary alloc] init];

        for (NSDictionary *dict in result) {
          Category *category = [[Category alloc] init];
          [category setValuesForKeysWithDictionary:dict];
          NSMutableArray *arrayInDict =
              [self.dic objectForKey:category.category];
          if (!arrayInDict) {
            arrayInDict = [[NSMutableArray alloc] init];
          }

          [arrayInDict addObject:category];
          [self.dic setObject:arrayInDict forKey:category.category];
        }

        for (NSMutableArray *arr in self.dic.allValues) {
          [self.categoryArr addObject:[[CollectionViewDataSource alloc]
                                          initWithCategory:arr]];
        }

        [self.activityIndicator stopAnimating];

        self.postCount++;

        if (self.postCount > 4) {
          self.postCount = 1;
        }

        self.zeekitImageView.hidden = YES;
        self.tableView.hidden = NO;
        self.titleLbl.hidden = NO;
        [self.tableView reloadData];
      }

      failure:^(NSError *_Nullable e) {
        [AlertHelper showAlertWithTitle:@"Oops"
                             andMessage:e.localizedDescription];
        [self.activityIndicator stopAnimating];

      }];
}

#pragma mark tableview datasource

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  CategoryTableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                      forIndexPath:indexPath];
  if (indexPath.row % 2 == 0) {
    cell.backgroundColor =
        [UIColor colorWithRed:0.82 green:0.34 blue:0.35 alpha:1.0];
  } else {
    cell.backgroundColor =
        [UIColor colorWithRed:0.84 green:0.45 blue:0.47 alpha:1.0];
  }

  if (cell == nil) {
    cell = [[CategoryTableViewCell alloc]
          initWithStyle:UITableViewCellStyleSubtitle
        reuseIdentifier:CellIdentifier];
  }

  [cell setDataSource:[self.categoryArr objectAtIndex:indexPath.row]];

  cell.delegate = self;

  return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [self.categoryArr count];
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)onCategorySelected:(Category *)category {
  // once user clicked the image, get another post

  UIStoryboard *mainStoryboard =
      [UIStoryboard storyboardWithName:@"Main" bundle:nil];

  ModalViewController *modalViewController =
      (ModalViewController *)[mainStoryboard
          instantiateViewControllerWithIdentifier:@"ModalViewController"];

  modalViewController.category = category;

  [self presentViewController:modalViewController animated:YES completion:nil];
}
@end
