//
//  ModalViewController.m
//  Zeekit Assignment
//
//  Created by Roei Baruch on 11/12/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ModalViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ModalViewController ()

@end

@implementation ModalViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  [self fillModalVcWithCategory:self.category];
}

- (void)fillModalVcWithCategory:(Category *)category {

  self.name.text = category.name;
  [self.imgView sd_setImageWithURL:[NSURL URLWithString:category.image_url]
                  placeholderImage:[UIImage imageNamed:@"placeHolder.png"]
                           options:SDWebImageRefreshCached];
}

- (IBAction)dismiss:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
